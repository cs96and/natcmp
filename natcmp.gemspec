Gem::Specification.new do |s|
	s.name             = 'natcmp'
	s.version          = '1.4.1'
	s.date             = '2011-11-09'
	s.summary          = 'Natural order string comparison module.'
	s.description      = 'Natural order string comparison is a way of comparing strings "naturally". e.g. "somthing1" < "something2" < "something10" which does not follow alpabetically.'
	s.authors          = ['Alan Davies']
	s.email            = 'alan n davies at gmail com'
	s.files            = ['lib/natcmp.rb', 'History.txt', 'README.rdoc']
	s.extra_rdoc_files = ['README.rdoc']
	s.homepage         = 'http://rubygems.org/gems/natcmp'
end
