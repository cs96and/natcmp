Gem::Specification.new do |s|
	s.name             = 'natcmp'
	s.version          = '1.4.3'
	s.date             = '2023-09-08'
	s.summary          = 'Natural order string comparison module.'
	s.description      = 'Natural order string comparison is a way of comparing strings "naturally". e.g. "somthing1" < "something2" < "something10" which does not follow alpabetically.'
	s.authors          = ['Alan Davies']
	s.email            = 'alan n davies at gmail com'
	s.files            = ['lib/natcmp.rb', 'History.txt', 'README.rdoc']
	s.extra_rdoc_files = ['README.rdoc']
	s.homepage         = 'https://gitlab.com/cs96and/natcmp'
	s.metadata         = { "source_code_uri" => "https://gitlab.com/cs96and/natcmp" }
end
